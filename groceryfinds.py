from flask import Flask, render_template, request, redirect, url_for, jsonify
import random

app = Flask(__name__)

models = {
    "items": [
        {"id": "1", "name": "strawberries", "quantity": "1lb", "price": "3.9", "description": "1 pound of fresh strawberries",
            "stores": ["walmart, kroger"], "locations":["austin", "dallas"], "url": "static/images/strawberries.png", "alt": "strawberries"},
        {"id": "2", "name": "milk", "quantity": "1gal", "price": "4.18", "description": "Pasteurized, Grade A whole milk with no artificial growth hormones",
            "stores": ["walmart, kroger, HEB"], "locations":["austin", "dallas", "houston"], "url": "static/images/milk.png", "alt": "milk"},
        {"id": "3", "name": "eggs", "quantity": "12ct", "price": "4.87", "description": "Cage free, large white eggs. Raised without antibiotics or hormones",
            "stores": ["walmart, HEB"], "locations":["austin", "houston"], "url": "static/images/eggs.png", "alt": "eggs"}],
    "stores": [
        {"id": "1", "name": "walmart", "description": "save money, live better", "rating": "4.5", "items": [
            "strawberries", "milk", "eggs"], "locations":["austin", "dallas", "houston"], "url": "static/images/walmart.png", "alt": "walmart", "web":"https://www.walmart.com/"},
        {"id": "2", "name": "kroger", "description": "fresh for everyone", "rating": "4.3", "items": [
            "strawberries", "milk"], "locations":["dallas", "houston"], "url": "static/images/kroger.png", "alt": "kroger", "web":"https://www.kroger.com/"},
        {"id": "3", "name": "HEB", "description": "here everything's better", "rating": "4.6", "items": [
            "milk", "eggs"], "locations":["austin", "houston"], "url": "static/images/heb.png", "alt": "heb", "web":"https://www.heb.com/"}
    ],
    "locations": [
        {"id": "1", "name": "austin", "description": "the capital of texas", "population": "964000",
            "items": ["strawberries", "milk", "eggs"], "stores":["walmart", "HEB"], "url": "static/images/austin.png", "alt": "austin"},
        {"id": "2", "name": "dallas", "description": "part of DFW", "population": "1300000",
            "items": ["strawberries", "milk"], "stores":["walmart", "kroger"], "url": "static/images/dallas.png", "alt": "dallas"},
        {"id": "3", "name": "houston", "description": "too damn big", "population": "2300000",
            "items": ["milk", "eggs"], "stores":["walmart", "HEB", "kroger"], "url": "static/images/houston.png", "alt": "houston"}
    ]
}

model_images = {
    "items": {"url": "static/images/items.png", "alt": "items"},
    "stores": {"url": "static/images/stores.png", "alt": "stores"},
    "locations": {"url": "static/images/locations.jpeg", "alt": "locations"}
}


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/model')
def model():
    return render_template('model.html', models=models, images=model_images)


@app.route('/model/<string:model_name>')
def model_attributes(model_name):
    return render_template('model_attributes.html', models=models, model_name=model_name)


@app.route('/items')
def items():
    items = models.get("items")
    pictures = model_images.get("items")
    return render_template('items.html', items=items, pictures=pictures)


@app.route('/store')
def store():
    return render_template('store.html', stores=models.get("stores"))


@app.route('/location')
def location():
    return render_template('location.html', locations=models.get("locations"))


@app.route('/about')
def about():
    return render_template('about.html')


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=3001)
